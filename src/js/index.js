$(document).ready(function(){

    $(".menu").on("click","a", function (event) {
        event.preventDefault();

        var id  = $(this).attr('href'),
            top = $(id).offset().top;

        $('body,html').animate({scrollTop: top}, 1500);
    });

    $(function() {
        var header = $(".header");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 500) {
                header.removeClass('header-static').addClass('header-scroll');
            } else {
                header.removeClass('header-scroll').addClass('header-static');
            }
        });
    });

});
